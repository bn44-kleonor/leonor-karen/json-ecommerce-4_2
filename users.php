<?php
require 'partials/header.php';

if(!isset($_SESSION["isAdmin"]) || $_SESSION["isAdmin"] == false){
    header("Location: index.php");
}

?>

<!-- USERS -->
<div class="container mb-5">
    <div class="row">
        <div class="col">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col" width="15%">#</th>
                        <th scope="col" width="20%">First Name</th>
                        <th scope="col" width="20%">Last Name</th>
                        <th scope="col" width="10%">Email</th>
                        <th scope="col" width="10%">Role</th>
                        <th scope="col" width="25%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        for($i=0; $i < count($users); $i++){
                    ?>
                    <tr>
                        <!-- User number -->
                        <td>
                            <?php echo $i+1; ?>
                        </td>
                        <!-- First Name iterate -->
                        <td>
                            <?php echo $users[$i]["firstname"]; ?>
                        </td>
                        <td>
                            <?php echo $users[$i]["lastname"]; ?>
                        </td>
                        <td>
                            <?php echo $users[$i]["email"]; ?></td>
                        <td>
                            <?php echo $users[$i]["isAdmin"]; ?>
                        </td>
                        <td>
                            <div class="d-flex flex-row">
                                <!-- READ -->
                                <a href="user.php?userid=<?php echo $i; ?>" class="btn btn-primary mr-1">View</a>

                                <!-- UPDATE -->
                                <a href="editrole.php?userid=<?php echo $i; ?>" class="btn btn-warning mr-1">Edit Role</a>

                                <!-- DELETE -->
                                <a href="assets/lib/processDeleteUser.php?userid=<?php echo $i; ?>" class="btn btn-danger mr-1">Delete</a>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php
require 'partials/footer.php';
?>