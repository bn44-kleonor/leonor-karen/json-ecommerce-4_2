<?php
session_start();
$product_name = $_POST["name"];
$price = $_POST["price"];
$price = floatval($price);
$desc = $_POST["description"];

//SYNTAX: $_FILES[name in the form][property]
// commonly used properties: name, tmp_name, size
$filename = $_FILES["hello"]["name"];
$file_tmpname = $_FILES["hello"]["tmp_name"];
$filesize = $_FILES["hello"]["size"];

$file_type = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
// $path = pathinfo($filename);
// var_dump($path["extension"]);

//validation
$hasDetails = false;
$isImg = false;

if($product_name != "" && $price > 0 && $desc != ""){
	$hasDetails = true;
}

if($file_type == "jpg" || $file_type == "png" || $file_type == "jpeg") {
	$isImg = true;
}

if($filesize > 0 && $isImg && $hasDetails) {
	// var_dump($file_tmpname);
	// exit();
	$final_path = "images/" . $filename;
	//move temporarily stored image to final file path
	move_uploaded_file($file_tmpname, $final_path);
} else {
	//create error message: Please upload an image
	$_SESSION["error_message"] = "Please upload an image";
	// redirect to addproduct.php
	header("Location: ../../addproduct.php");
}

//create a php associative array
$new_product = [
	"name" => $product_name,
	"price" => $price,
	"description" => $desc,
	"image" => $final_path 
];

//access contents of products.json
$products_objects = file_get_contents('products.json');

//convert contents to php array
$products = json_decode($products_objects, true);

//push new product
array_push($products, $new_product);

//open json file for writing
$to_write = fopen('products.json', 'w');

//write to the opened json file
fwrite($to_write, json_encode($products, JSON_PRETTY_PRINT));

//close
fclose($to_write);

$_SESSION["message"] = "$product_name has been successfully added!";
header("Location: ../../products.php");
?>