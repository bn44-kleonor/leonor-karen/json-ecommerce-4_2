<?php
session_start();
$firstname = $_POST["firstname"];
$lastname = $_POST["lastname"];
$email = $_POST["email"];
$role = $_POST["role"];

//access users.json (ni-access kasi this file does not containt the 'partials/header.php')
$users_objects = file_get_contents("users.json");
$users = json_decode($users_objects, true);
$i = $_GET["userid"];

//update properties of product
$users[$i]["firstname"] = $firstname;
$users[$i]["lastname"] = $lastname;
$users[$i]["email"] = $email;
$users[$i]["isAdmin"] = $role;

//open json file for writing
$to_write = fopen('users.json', 'w');

//write to the opened json file
fwrite($to_write, json_encode($users, JSON_PRETTY_PRINT));

//close
fclose($to_write);

$_SESSION["message"] = "$firstname has been successfully updated!";
header("Location: ../../editrole.php?userid=${i}");

?>

<!-- delete?
url productid
true?
include header? -->
