<?php 
session_start();

$users_objects = file_get_contents("users.json");
$users = json_decode($users_objects, true);
$i = $_GET["userid"];
$firstname = $users[$i]["firstname"];
$lastname = $users[$i]["lastname"];

//delete the user
unset($users[$i]);
$users2 = array_values($users);

//open
$to_write = fopen("users.json", "w");

//write on opened file
fwrite($to_write, json_encode($users2, JSON_PRETTY_PRINT));

//close
fclose($to_write);

$_SESSION["message"] = "$firstname $lastname has been successfully deleted!";
header("Location: ../../users.php");
die();

?>