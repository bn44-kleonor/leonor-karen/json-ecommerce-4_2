<?php
	session_start();

	$products_objects = file_get_contents("products.json");
	$products = json_decode($products_objects, true);
	$i = $_GET["productid"];
	$product_name = $products[$i]["name"];
	$product_image = $products[$i]["image"];


	//delete image
	unlink($product_image);

	//deleting the product array/obj itself
	//var_dump($products);
	unset($products[$i]);
	$products2 = array_values($products);
	//var_dump($products);
	//die();

	//open
	$to_write = fopen("products.json", "w");

	//write on opened file
	fwrite($to_write, json_encode($products2, JSON_PRETTY_PRINT));

	//close
	fclose($to_write);

	$_SESSION["message"] = "$product_name has been successfully deleted!";
	header("Location: ../../products.php");
	die();

?>
