<?php
require 'partials/header.php';
$i = $_GET["userid"];
?>

<!-- USER -->
<div class="container">
    <div class="row">
        <div class="col md-1"></div>
        <div class="col-md-3">
            <!-- IMAGE -->
            <div class="card mb-4">
                <img class="card-img-top" src="assets/lib/<?php echo $users[$i]["image"]; ?>" alt="profile image">
            </div>
        </div>
        <div class="col-md-7">
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="card-title">
                        <?php echo $users[$i]["firstname"] ?>
                        <?php echo ", " ?>
                        <?php echo $users[$i]["lastname"] ?>
                    </h5>
                    <h5 class="card-title text-secondary">
                        <?php echo $users[$i]["email"] ?>
                    </h5>
                    <h5 class="card-title text-secondary">
                        <?php echo $users[$i]["role"] ?>
                    </h5>
                </div>
            </div>
        </div>
        <div class="col md-1"></div>
    </div>
</div>


<?php
require 'partials/footer.php';
?>
