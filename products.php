<?php
require 'partials/header.php';  
//'require' means the next codes will not run until you have the 'header.php'
// var_dump(isset($_SESSION["isAdmin"])); 
// var_dump($_SESSION["isAdmin"] == false);
//var_dump(!isset($_SESSION["isAdmin"]) || $_SESSION["isAdmin"] == false);
// die();
//false if not logged in
//true if logged in
if(!isset($_SESSION["isAdmin"]) || $_SESSION["isAdmin"] == false){
    header("Location: index.php");
}

?>

<!-- ADMIN -->
<div class="container mb-5">
    <div class="row">
        <div class="col">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col" width="25%">#</th>
                        <th scope="col" width="25%">Name</th>
                        <th scope="col" width="25%">Price</th>
                        <th scope="col" width="25%">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        for($i=0; $i < count($products); $i++){
                    ?>
                    <tr>
                        <!-- product number -->
                        <td><?php echo $i + 1; ?></td>
                        <!-- product name -->
                        <th scope="row">
                            <?php
                                echo $products[$i]["name"];
                            ?>
                        </th>
                        <!-- product price -->
                        <td>
                            &#8369;
                            <?php
                                echo number_format($products[$i]["price"], 2, ".", "");
                            ?>
                        <!-- Product action | View, Edit, Delete -->
                        <td>
                            <div class="d-flex flex-row">
                                <!-- READ -->
                                <a href="product.php?productid=<?php echo $i; ?>" class="btn btn-primary mr-1">View</a>

                                <!-- UPDATE -->
                                <a href="editproduct.php?productid=<?php echo $i; ?>" class="btn btn-warning mr-1">Edit</a>

                                <!-- DELETE -->
                                <a href="assets/lib/processDeleteProduct.php?productid=<?php echo $i; ?>" class="btn btn-danger mr-1">Delete</a>
                            </div>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <a href="addproduct.php" class="btn btn-success">Add Product</a>
        </div>
    </div>
</div>

<?php
require 'partials/footer.php';
?>