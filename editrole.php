<?php 
require "partials/header.php";
//userid has been appended on the URL: GET method gets the userid from the URL
$i = $_GET["userid"];
?>

<form method="POST" action="assets/lib/processUpdateRole.php?userid=<?php echo $i; ?>" class="d-flex flex-row" enctype="multipart/form-data">
	<div class="container">
		<div class="row">
			<div class="col-6">
				<img src="assets/lib/<?php echo $users[$i]["image"]; ?>" alt="Card image">
			</div>
			<div class="col-6">
                <div class="form-group">
                    <label for="name">Firstname</label>
                    <input type="text" class="form-control" name="firstname" id="firstname" required value="<?php echo $users[$i]["firstname"]; ?>">
                </div>
                <div class="form-group">
                    <label for="name">Lastname</label>
                    <input type="text" class="form-control" name="lastname" id="lastname" required value="<?php echo $users[$i]["lastname"]; ?>">
                </div>
                <div class="form-group">
                    <label for="name">Email</label>
                    <input type="text" class="form-control" name="email" id="email" required value="<?php echo $users[$i]["email"]; ?>">
                </div>
                <div class="form-group">
                    <label for="name">Role</label>
                    <input type="text" class="form-control" name="role" id="role" required value="<?php echo $users[$i]["isAdmin"]; ?>">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="users.php" class="btn btn-secondary">Back</a>
			</div>
		</div>
	</div>
</form>

<?php
require "partials/footer.php";
?>
